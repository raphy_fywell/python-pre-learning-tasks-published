def vowel_swapper(string):
    # ==============

    new_string = string
    counter = 0
    for char in string.lower():
        if char == 'a':
            counter += 1

    if counter > 1:
        index = 0
        for _ in range(2):
            index += string.lower()[index:].index('a') + 1

        new_string = string[:index-1] + '/\\' + string[index:]

    counter = 0
    for char in new_string.lower():
        if char == 'e':
            counter += 1

    if counter > 1:
        index = 0
        for _ in range(2):
            index += new_string.lower()[index:].index('e') + 1

        new_string = new_string[:index - 1] + '3' + new_string[index:]

    counter = 0
    for char in new_string.lower():
        if char == 'i':
            counter += 1

    if counter > 1:
        index = 0
        for _ in range(2):
            index += new_string.lower()[index:].index('i') + 1

        new_string = new_string[:index - 1] + '!' + new_string[index:]

    counter = 0
    for char in new_string.lower():
        if char == 'u':
            counter += 1

    if counter > 1:
        index = 0
        for _ in range(2):
            index += new_string.lower()[index:].index('u') + 1

        new_string = new_string[:index - 1] + '\\/' + new_string[index:]

    counter = 0
    for char in new_string.lower():
        if char == 'o':
            counter += 1

    if counter > 1:
        index = 0
        for _ in range(2):
            index += new_string.lower()[index:].index('o') + 1

        if new_string[index-1] == 'o':
            new_string = new_string[:index - 1] + 'ooo' + new_string[index:]
        elif new_string[index-1] == 'O':
            new_string = new_string[:index - 1] + 'OOO' + new_string[index:]

    return new_string

    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
